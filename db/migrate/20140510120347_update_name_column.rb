class UpdateNameColumn < ActiveRecord::Migration
  def change
    change_table :events do |t|
      t.remove :data
      t.datetime :date
    end
  end
end
