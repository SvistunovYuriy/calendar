class UsersController < ApplicationController
  before_action :current_user,only: [:show,:edit,:update] 
  after_action :reset_session,only: [:destoy,:log_out]

  def show 
    @event = Event.new
  end

  def start
  end

  def sign_in
    @user = User.find_by(sign_in_params)
    if @user
      session[:id] = @user.id
      redirect_to home_path
    else
      flash[:error] = "password or email wrong"
      render :start
    end
  end

  def new
   @user = User.new 
  end

  def create
    @user = User.create(sign_up_params)
    if @user.save
      session[:id] = @user.id
      redirect_to home_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(sing_up_params)
      flash[:notice] = "You seccessfull have updated yours profile"
      redirect_to :show
    else 
      render :edit
    end
  end

  def log_out
    redirect_to sign_in_user_path
  end

  def destroy
    @user.destroy
    redirect_to new_user_path
  end

    protected
      def sign_in_params
        params.require(:sign_in).permit(:email,:password)
      end
      def sign_up_params
        params.require(:user).permit(:email,:password,:password_confirmation,:name)
      end
end
