class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :require_login,except: [:start,:sign_in,:new,:create]

  protected 
    def require_login
      redirect_to sign_in_user_path unless current_user
    end
    def reset_session
      session[:id] = nil
    end

    def current_user
      @user ||= session[:id]&&User.find_by(id: session[:id])
    end
end
